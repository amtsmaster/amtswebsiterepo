//Modules required from core and package.json
const express     = require('express');
const path        = require('path'); //core module
const bodyParser  = require('body-parser');
const cors        = require('cors');
const passport    = require('passport');
const mongoose    = require('mongoose');
const config      = require('./config/database');

mongoose.connect(config.database); //connect to mongoose database
mongoose.connection.on('connected', () => {     //tell us if we are connected
  console.log('Connected to database '+config.database)
});

mongoose.connection.on('error', (error) => {     //tell the error if it occurs
  console.log('Database Error'+error)
});

const users = require('./routes/users'); //contains routes that users can access

const app   = express();
const port  = 3000; //port number

app.use(cors());              //cors middleware
app.use(bodyParser.json());   //bodyParser middleware

app.use(passport.initialize()); //passport middleware got authentication and token generation
app.use(passport.session());   //will use passport-jwt strategy
require('./config/passport')(passport);

app.use('/users', users);      //users routes
app.use(express.static(path.join(__dirname, 'public'))); //static angular app folder

//Index Route
app.get('/', (request, response) => { //route set root Endpoint
  response.send('Invalid Endpoint!');
});

app.get('*', (request, response) => {
  response.sendFile(path.join(__dirname, 'public/Index.html')); //all routes go to here
});

//server start
app.listen(port, () => {
  console.log('Server started on port ' + port);
});
