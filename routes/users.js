//Add all routes that users can access here
const express   = require('express');
const User      = require('../models/user');
const passport  = require('passport');
const jwt       = require('jsonwebtoken');
const config    = require('../config/database');
const router    = express.Router();

/////////////////////add passport.authenticate('jwt', {session: false}) as a parameter to protect a route.

//Register
router.post('/register', (request, response, next) => {
  let newUser = new User({
    name    : request.body.name,
    email   : request.body.email,
    username: request.body.username,
    password: request.body.password
  });

  User.addUser(newUser, (error, user) => {
    if(error) {
      response.json({ success: false, msg:'Failed To Register User'});
    } else {
      response.json({ success: true, msg:'User Registered'});
    }
  });
});

//Authenticate
router.post('/authenticate', (request, response, next) => {
  const username = request.body.username;
  const password = request.body.password;

  User.getUserByUsername(username, (error, user) => {
    if(error) throw error;
    if(!user) {
      return response.json({success: false, msg: 'User not found :('});
    } //when user is not in db

    //otherwise assume it is, check the rest of the stuff
    User.comparePassword(password, user.password, (error, isMatch) => {
      if(error) throw error;
      if(isMatch) {
        //create the token
        const token = jwt.sign(user, config.secret, {
          expiresIn: 3600 //1 day
        });

        response.json({
          success : true,
          token   : 'JWT '+token,
          user    : {               //send back data for a profile or something
            id      : user._id,
            name    : user.username,
            email   : user.email
          }
        });
      } else {
        //no match
        return response.json({success:false, msg: 'Incorrect Password'});
      }
    });

  });
});

//Profile
router.get('/profile', passport.authenticate('jwt', {session: false}), (request, response, next) => {
  response.json({user:request.user});
});

module.exports = router;
